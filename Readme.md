# To execute from Run-xxx folder
* Use RunFile-01.submit and 
* on the output execute RunFile-02.submit

Requires 
- InputParam-01.txt
- silva.v4.8mer
- trainset9_032012.pds.fasta
- trainset9_03012.pds.tax


# To execute from folder containing fastq files of interest
* Use RunFile-03.submit
* followed by RunFile-02.submit

Requires 
- SampleGroup.file
- silva.v4.8mer
- trainset9_032012.pds.fasta
- trainset9_03012.pds.tax